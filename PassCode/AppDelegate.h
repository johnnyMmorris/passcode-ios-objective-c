//
//  AppDelegate.h
//  PassCode
//
//  Created by Morris, Johnny on 11/16/16.
//  Copyright © 2016 Johnny Morris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

