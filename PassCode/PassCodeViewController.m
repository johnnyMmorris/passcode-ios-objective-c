//
//  ViewController.m
//  PassCode
//
//  Created by Morris, Johnny on 11/16/16.
//  Copyright © 2016 Johnny Morris. All rights reserved.
//

#import "PassCodeViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface PassCodeViewController ()
@property (nonatomic, strong) NSMutableString *ms;
@property BOOL isFirstTime;
@end

@implementation PassCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.ms = [NSMutableString stringWithString:@""];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults boolForKey:@"isFirstTime"] == YES || ![defaults valueForKey:@"isFirstTime"]) {
        self.isFirstTime = YES;
    } else {
        self.isFirstTime = NO;
    }
    NSString *passCode = [defaults valueForKey:@"PassCode"];
    NSLog(@"%@", passCode);
}

-(void)viewWillAppear:(BOOL)animated {
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @" ";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSLog(@"PassCode validation Pass");
                                        self.lockImage.image = [UIImage imageNamed:@"UnLocked"];
                                        UIAlertController *passedAlert = [UIAlertController alertControllerWithTitle:@"Passcode Vaidation Passed" message:@"you can Present your secure view controller instead of showing the alert message" preferredStyle:UIAlertControllerStyleAlert];
                                        
                                        UIAlertAction *ok = [UIAlertAction
                                                             actionWithTitle:@"Ok"
                                                             style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action){
                                                                 self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
                                                                 self.enterPassCodeLabel.text = @"Enter PassCode";
                                                                 self.lockImage.image = [UIImage imageNamed:@"Locked"];
                                                                 [self viewDidLoad];
                                                             }];
                                        
                                        [passedAlert addAction:ok];
                                        [self presentViewController:passedAlert animated:YES completion:nil];
                                    });
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        
                                        // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
                                    });
                                }
                            }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                message:@"if the touch Id is not working, please enter your passcode."
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil, nil];
//            [alertView show];
            // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
        });
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)numberPressed:(UIButton *)sender {
    
    
    if (self.ms.length < 4) {
        [self.ms insertString:sender.currentTitle atIndex:self.ms.length];
    }
    
    
    
    NSLog(@"%@", self.ms);
    

        self.passCodeTextfield.text = self.ms;
        if (self.ms.length == 1) {
            self.passCodeImage.image = [UIImage imageNamed:@"Dots1"];
        }
        if (self.ms.length == 2){
            self.passCodeImage.image = [UIImage imageNamed:@"Dots2"];
        }
        if (self.ms.length == 3){
            self.passCodeImage.image = [UIImage imageNamed:@"Dots3"];
        }
        if (self.ms.length == 4){
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            self.passCodeImage.image = [UIImage imageNamed:@"Dots4"];
            if (self.isFirstTime == YES) {
                self.enterPassCodeLabel.text = @"Re-Enter PassCode";
                self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
                [defaults setObject:self.ms forKey:@"PassCode"];
                [defaults setBool:NO forKey:@"isFirstTime"];
                [defaults synchronize];
                self.ms = [NSMutableString stringWithString:@""];
                self.isFirstTime = NO;
            } else {
                if ([self.ms isEqualToString:[defaults stringForKey:@"PassCode"]]) {
                    NSLog(@"PassCode validation Pass");
                    self.lockImage.image = [UIImage imageNamed:@"UnLocked"];
                    UIAlertController *passedAlert = [UIAlertController alertControllerWithTitle:@"Passcode Vaidation Passed" message:@"you can Present your secure view controller instead of showing the alert message" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction
                                             actionWithTitle:@"Ok"
                                             style:UIAlertActionStyleCancel
                                             handler:^(UIAlertAction *action){
                                                 self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
                                                 self.enterPassCodeLabel.text = @"Enter PassCode";
                                                 self.lockImage.image = [UIImage imageNamed:@"Locked"];
                                                 [self viewDidLoad];
                                             }];
                    
                    [passedAlert addAction:ok];
                    [self presentViewController:passedAlert animated:YES completion:nil];
                    
                } else if (self.ms.length == 4 && ![self.ms isEqualToString:[defaults stringForKey:@"PassCode"]]){
                    self.enterPassCodeLabel.text = @"Wrong PassCode, Re-Enter again";
                    self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
                    self.ms = [NSMutableString stringWithString:@""];
                }
                
            }
        }
    
    
}

- (IBAction)deletePressed:(id)sender {
    
    if (self.ms.length > 0) {
        [self.ms deleteCharactersInRange:NSMakeRange([self.ms length]-1, 1)];
    }
    
    
    NSLog(@"%@", self.ms);
        
    
    self.passCodeTextfield.text = self.ms;
    if (self.ms.length == 0) {
        self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
    }
    if (self.ms.length == 1) {
        self.passCodeImage.image = [UIImage imageNamed:@"Dots1"];
    }
    if (self.ms.length == 2){
        self.passCodeImage.image = [UIImage imageNamed:@"Dots2"];
    }
    if (self.ms.length == 3){
        self.passCodeImage.image = [UIImage imageNamed:@"Dots3"];
    }
    if (self.ms.length == 4){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        self.passCodeImage.image = [UIImage imageNamed:@"Dots4"];
        if (self.isFirstTime == YES) {
            self.enterPassCodeLabel.text = @"Re-Enter PassCode";
            self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
            [defaults setObject:self.ms forKey:@"PassCode"];
            [defaults setBool:NO forKey:@"isFirstTime"];
            [defaults synchronize];
            self.ms = [NSMutableString stringWithString:@""];
            self.isFirstTime = NO;
        } else {
            if ([self.ms isEqualToString:[defaults stringForKey:@"PassCode"]]) {
                //Add your view controller
                NSLog(@"PassCode validation Pass");
                
            } else if (self.ms.length == 4 && ![self.ms isEqualToString:[defaults stringForKey:@"PassCode"]]){
                self.enterPassCodeLabel.text = @"Wrong PassCode, Re-Enter again";
                self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
                self.ms = [NSMutableString stringWithString:@""];
            }
            
        }
    }
}

- (IBAction)resetPressed:(id)sender {
    
    UIAlertController *resetAlert = [UIAlertController alertControllerWithTitle:@"Reset Password" message:@"The Reset button will reset the password. you can handle reseting the passcode." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *reset = [UIAlertAction
                            actionWithTitle:@"Reset"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction *action){
                                //here where you handle the reset
                                
                                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                [defaults setBool:YES forKey:@"isFirstTime"];
                                [defaults setObject:@"" forKey:@"PassCode"];
                                [defaults synchronize];
                                self.passCodeImage.image = [UIImage imageNamed:@"Dots0"];
                                self.lockImage.image = [UIImage imageNamed:@"Locked"];
                                self.enterPassCodeLabel.text = @"Enter New PassCode";
                                [self viewDidLoad];

                                
                            }];
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action){
                                 
                             }];
    
    [resetAlert addAction:reset];
    [resetAlert addAction:cancel];
    [self presentViewController:resetAlert animated:YES completion:nil];
    
}


@end
