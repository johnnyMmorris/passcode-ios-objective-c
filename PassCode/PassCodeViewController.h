//
//  ViewController.h
//  PassCode
//
//  Created by Morris, Johnny on 11/16/16.
//  Copyright © 2016 Johnny Morris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PassCodeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *enterPassCodeLabel;
@property (strong, nonatomic) IBOutlet UITextField *passCodeTextfield;
@property (strong, nonatomic) IBOutlet UIImageView *passCodeImage;
@property (strong, nonatomic) IBOutlet UIImageView *lockImage;


- (IBAction)numberPressed:(id)sender;
- (IBAction)deletePressed:(id)sender;
- (IBAction)resetPressed:(id)sender;


@end

