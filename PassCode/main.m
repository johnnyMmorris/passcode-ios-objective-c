//
//  main.m
//  PassCode
//
//  Created by Morris, Johnny on 11/16/16.
//  Copyright © 2016 Johnny Morris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
